package com.app.projetofinal.Fragments


import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.Toast
import com.app.projetofinal.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.frag_teste1.*
import kotlinx.android.synthetic.main.frag_teste1.view.*
import kotlinx.android.synthetic.main.frag_searchview.view.*
import kotlinx.android.synthetic.main.frag_teste1.view.imagePesquisa
import kotlinx.android.synthetic.main.frag_teste1.view.titulo_narrativa


class FragmentTeste1 : androidx.fragment.app.Fragment(){
    private lateinit var langHelper: LangHelper
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.frag_teste1, container, false)
        var jsonarray = arguments?.getString("timeline")
        var jsonarrayDomains = arguments?.getString("domains")

        var years = arguments?.getString("years")
        var query = arguments?.getString("query")

        var gson = Gson()
        val turnsType = object : TypeToken<List<Timeline>>() {}.type
        var testModel = gson.fromJson<List<Timeline>>(jsonarray, turnsType)

        langHelper = LangHelper(activity!!.applicationContext)



        view.titulo_narrativa.setOnFocusChangeListener { _, _ ->  view.imagePesquisa.setImageResource(R.drawable.ic_search_black_24dp) }

        view.titulo_narrativa.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                view.titulo_narrativa.hideKeyboard()
                view.linear_aux.requestFocus()


                //Perform Code
                return@OnKeyListener true
            }
            false
        })


        if(langHelper.getLanguageSaved().equals("en")) {
            //view.titulo_narrativa.text= "Search: "+query
            //view.titulo_narrativa.text=Html.fromHtml("Search: "+"<u>"+query+"</u>")
            view.titulo_narrativa.setText(query)
        }
        else{
            view.titulo_narrativa.setText(query)
            //view.titulo_narrativa.hint = query
            //view.titulo_narrativa.text=Html.fromHtml("Pesquisa: "+"<u>"+query+"</u>")
        }

        view.imagePesquisa.setOnClickListener {


            val manager = activity?.applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = manager.activeNetworkInfo
            if (null != activeNetwork) {
                if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                    var aux = view.titulo_narrativa.text.toString()

                    val result = aux.trim()

                    view.imagePesquisa.hideKeyboard()

                    if(result.isNotEmpty()){
                        val kotlinFragment = FragmentOne.newInstance(result,10)

                        (activity as SecondActivity).replaceFragment(kotlinFragment)
                    }
                    else{
                        buttonerrorempty(view)
                    }
                }
                if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                    var aux = view.titulo_narrativa.text.toString()

                    val result = aux.trim()

                    //var aux2 = listItemsTxt.get(view.spinner1.selectedItemPosition)
                    view.imagePesquisa.hideKeyboard()

                    if(result.isNotEmpty()){
                        val kotlinFragment = FragmentOne.newInstance(result,10)

                        (activity as SecondActivity).replaceFragment(kotlinFragment)
                    }
                    else{
                        buttonerrorempty(view)
                    }
                }
            } else {
                buttonnointernet(view)
            }
        }

        view.step_view.setOnClickListener {
            if(langHelper.getLanguageSaved().equals("en")){
                val toast = Toast.makeText(
                    activity?.applicationContext,
                    R.string.ajuda_narrativa_en, Toast.LENGTH_LONG
                )
                toast.setGravity(Gravity.TOP or Gravity.CENTER_VERTICAL, 0, 70)
                toast.show()
            }
            else{
                val toast = Toast.makeText(
                    activity?.applicationContext,
                    R.string.ajuda_narrativa, Toast.LENGTH_LONG
                )
                toast.setGravity(Gravity.TOP or Gravity.CENTER_VERTICAL, 0, 70)
                toast.show()
            }


            //Toast.makeText(context,getString(R.string.ajuda_narrativa),Toast.LENGTH_LONG).show()
        }

        val adapter = ViewPagerAdapterNarrativas(childFragmentManager)

        val emptyList = mutableListOf<String>()

        for(item in testModel){
            var jsonString = gson.toJson(item.headlines)
            var fragmento = FragmentVistaNarrativas.newInstance(jsonString,item.date_interval_end,item.date_interval_begin,
                query!!, jsonarrayDomains!!,years!!
            )
            emptyList.add("")
            adapter.addFragment(fragmento)
        }
        view.viewpagernarr.adapter = adapter
        //val a = listOf<String>("03/17","23/18","05/19","06/19","06/19")
        view.step_view.setSteps(emptyList)
        view.step_view.selectedStep(0)


        view.viewpagernarr.addOnPageChangeListener(object : androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                var aux = view.viewpagernarr.currentItem
                var tam = view.viewpagernarr.adapter?.count



                if(aux==0){
                    step_view.selectedStep(0)
                }else{
                    step_view.selectedStep(aux+1)
                }
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
            }
        })

        return view

    }


    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    fun buttonerrorempty(view: View) {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.layout_mensagem_pesquisar_vazio)
        dialog.window.setBackgroundDrawableResource(android.R.color.transparent)

        val button = dialog.findViewById(R.id.buttonOk) as Button

        button.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    fun buttonnointernet(view: View) {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.layout_error_no_internet)
        dialog.window.setBackgroundDrawableResource(android.R.color.transparent)

        val button = dialog.findViewById(R.id.buttonOk) as Button

        button.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    fun convertedata(data: String):String{
        val strs = data.split("/").toTypedArray()

        val aux = strs.get(2)
        val aux2= aux.takeLast(2)
        return  strs.get(1)+"/"+aux2
    }

    companion object {
        fun newInstance(timeline:String, query : String , years : String , domains : String): FragmentTeste1 {
            val args = Bundle()
            args.putString("timeline",timeline)
            args.putString("query",query)
            args.putString("years",years)
            args.putString("domains",domains)
            val fragment = FragmentTeste1()
            fragment.arguments = args
            return fragment
        }
    }

}
